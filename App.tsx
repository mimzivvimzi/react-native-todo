import { StatusBar } from "expo-status-bar";
import {
  StyleSheet,
  Text,
  TextInput,
  View,
  Button,
  FlatList,
} from "react-native";
import * as LocalAuthentication from "expo-local-authentication";
import { useState, useEffect } from "react";
import { Entypo } from "@expo/vector-icons";
import uuid from "react-native-uuid";
import { Feather } from "@expo/vector-icons";
import { MaterialCommunityIcons } from "@expo/vector-icons";

export interface TodoItem {
  id: any;
  desc: string;
  completed: boolean;
}

export default function App() {
  const [isAuthenticated, setIsAuthenticated] = useState<boolean>(false);
  const [inputText, setInputText] = useState<string>("");
  const [todo, setTodo] = useState<TodoItem[]>([]);

  useEffect(() => {
    async function authenticate() {
      const result = await LocalAuthentication.authenticateAsync();
      setIsAuthenticated(result.success);
    }
    authenticate();
  }, []);

  const addItem = (inputText: string) => {
    setTodo([...todo, { id: uuid.v4(), desc: inputText, completed: false }]);
    setInputText("");
  };

  const toggleItemCompletion = (selectedItem: TodoItem) => {
    const updatedTodo = todo.map((item) => {
      if (item.id === selectedItem.id) {
        return { ...item, completed: !item.completed };
      }
      return item;
    });
    setTodo(updatedTodo);
  };

  const deleteItem = (selectedItem: TodoItem) => {
    const updatedTodo = todo.filter((item) => item.id !== selectedItem.id);
    setTodo(updatedTodo);
  };

  const renderTodo = (item: TodoItem) => {
    return (
      <View style={styles.item} key={item.id}>
        <View style={styles.listContainer}>
          <MaterialCommunityIcons
            name={item.completed ? "checkbox-marked" : "checkbox-blank-outline"}
            size={24}
            onPress={() => toggleItemCompletion(item)}
            color="#333333"
          />
          <Text>{item.desc}</Text>
        </View>
        <Feather
          name="trash-2"
          size={24}
          color="black"
          onPress={() => deleteItem(item)}
        />
      </View>
    );
  };

  return (
    <>
      {isAuthenticated ? (
        <>
          <View style={styles.header}>
            <Text style={styles.headerText}>To do app</Text>
          </View>
          <View style={styles.container}>
            <View style={styles.form}>
              <TextInput
                style={styles.input}
                placeholder="Enter your to-do list item here"
                onChangeText={(text) => setInputText(text)}
                value={inputText}
              />
              <Entypo
                name="plus"
                size={36}
                color="#333333"
                onPress={() => addItem(inputText)}
              />
            </View>
            {todo.map((item) => renderTodo(item))}
          </View>
        </>
      ) : (
        <View style={styles.accessDenied}>
          <Text>Access Denied</Text>
        </View>
      )}
    </>
  );
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: "#B4D2BA",
    flex: 1,
  },
  header: {
    backgroundColor: "#fff",
    marginTop: 60,
    alignItems: "center",
  },
  headerText: {
    color: "black",
    fontSize: 18,
    fontWeight: "500",
    marginBottom: 20,
  },
  form: {
    marginTop: 16,
    gap: 8,
    padding: 8,
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
  },
  input: {
    flex: 2,
    borderColor: "#333333",
    borderWidth: 2,
    borderRadius: 6,
    height: 50,
    padding: 10,
    backgroundColor: "#fff",
  },
  item: {
    padding: 8,
    marginVertical: 8,
    marginHorizontal: 8,
    borderWidth: 2,
    borderRadius: 6,
    height: 50,
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
    backgroundColor: "#D8DBE2",
  },
  listContainer: {
    flexDirection: "row",
    alignItems: "center",
    gap: 4,
  },
  accessDenied: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
  },
});
